public class BubbleThread extends Thread {
  public Integer[] array;
  public int threadNumber;

  public Integer[] getArray () {
    return this.array;
  }

  public BubbleThread(int threadNumber, Integer[] array) {
    this.threadNumber = threadNumber;
    this.array = array;
  }

  @Override
  public void run() {
    System.out.println("iniciando thread " + this.threadNumber);
    int n = this.array.length;
    int temp = 0;
    for (int i = 0; i < n; i++) {
      for (int j = 1; j < (n - i); j++) {
        if (this.array[j - 1] > this.array[j]) {
          temp = this.array[j - 1];
          this.array[j - 1] = this.array[j];
          this.array[j] = temp;
        }
      }
    }
  }
}