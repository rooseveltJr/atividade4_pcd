import java.util.*;

public class Main {
  
  public static Integer[] createArray(int size) {
    Integer[] array = new Integer[size];
    for (int i = 0; i < size; i++) {
      array[i] = size - i;
    }

    return array;
  }

  public static int getSplitFactor(Integer[] array, int threadsNumber) {
    int max = array[0];
    for (int i = 1; i < array.length; i++) {
      if (array[i] > max) max = array[i];
    }
    int splitFactor = max/threadsNumber;
    return splitFactor;
  }

  public static List<Integer>[] createSubLists(int threadsNumber) {
    List<Integer>[] sublists = new List[threadsNumber];
    for (int i = 0; i < threadsNumber; i++) sublists[i] = new ArrayList<Integer>();
    return sublists;
  }

  public static void splitArrayInSublists(Integer[] array, List<Integer>[] sublists, int splitFactor, int threadsNumber) {
    List<Integer> arrayList = new LinkedList(Arrays.asList(array));
    List<Integer> indexesToRemove = new ArrayList<Integer>();

    for (int i = 0; i < threadsNumber; i++) {
      for (int j = 0; j < arrayList.size(); j++) {
        if(arrayList.get(j) <= splitFactor*(i+1)) {
          sublists[i].add(arrayList.get(j));
          indexesToRemove.add(j);
        }
      }

      for (int j = indexesToRemove.size() - 1; j >= 0 ; j--) {
        int index = indexesToRemove.get(j);
        arrayList.remove(index);
      }
      indexesToRemove.clear();
    }
    if(arrayList.size() > 0) sublists[threadsNumber - 1].add(arrayList.get(0));
  }

  static List<Integer> concatAllSubLists (BubbleThread[] myThreads, int size) {
    List<Integer> result = new ArrayList<Integer>();
    for (int i = 0; i < myThreads.length; i++) {
      Integer[] subList = myThreads[i].getArray();
      for (int j = 0; j < subList.length; j++) {
        result.add(subList[j]);
      }
    }

    return result;
  }

  static void testOrdenation (List<Integer> array) {
    for (int i = 0; i < array.size() - 1; i++) {
      if (array.get(i) > array.get(i+1)) {
        System.out.println("\n\nNAO ORDENADO!!!!!");
        return;
      }
    }

    System.out.println("\n\nARRAY ORDENADO!!!!!");
    System.out.println("\n\nArray SIZE: " + array.size());
  }

  public static void main (String[] args) {
    int threadsNumber = 8;
    int size = 100000;
    Integer[] array = createArray(size);
    long startSequencialPart = System.currentTimeMillis();
    int splitFactor = getSplitFactor(array, threadsNumber);
    List<Integer>[] subLists = createSubLists(threadsNumber);
    splitArrayInSublists(array, subLists, splitFactor, threadsNumber);
    long endSequencialPart = System.currentTimeMillis();
    BubbleThread myThreads[] = new BubbleThread[threadsNumber];
    long startParallelPart = System.currentTimeMillis();
    for (int i = 0; i < threadsNumber; i++) {
      myThreads[i] = new BubbleThread(i, subLists[i].toArray(new Integer[0]));
      myThreads[i].start();
    }

    for (Thread thread : myThreads) {
      try {
        thread.join();
      } catch (InterruptedException e) {
      }
    }

    long endParallelPart = System.currentTimeMillis();
    List<Integer> result = concatAllSubLists(myThreads, size);
    System.out.println("Tempo na etapada sequencial: " + (double)(endSequencialPart - startSequencialPart)/1000);
    System.out.println("Tempo na etapada paralela: " + (double)(endParallelPart - startParallelPart)/1000);
    testOrdenation(result);
  }
}