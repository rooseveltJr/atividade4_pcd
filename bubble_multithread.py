import time
import random
import threading
import numpy as np

lock = threading.Lock()

def troca(array, i):
	array[i], array[i+1] = array[i+1], array[i]

def bubble(array):
        lock.acquire()
        n = len(array)
        
        for i in range(n):
            swap = False
            for j in range(0, n-i-1):
                if array[j]>array[j+1]:
                    troca(array, j)
                    swap = True
            if swap == False:
                break
        
        lock.release()

def bubble_paralelo(array): 
        n_threads = 4
        lists = [[] for _ in range(n_threads)]

        n = len(array)
        maior = max(array)
        split = maior//n_threads

        for j in range(1,len(lists)):
            for i in array:
                if i <= (split*j):
                    lists[j-1].append(i)
                    array.remove(i)
            lists[-1] = array

        active_threads = []

        #medição de tempo para trecho paralelo
        inicio = time.time()
        for list_item in lists:
            t = threading.Thread(target=bubble, args=(list_item,))
            t.start()
            active_threads.append(t)
            
        for thread in active_threads:
            thread.join()
        
        array_final = []
        for i in range(n_threads):
            array_final.append(lists[i])
        final = time.time()
        print(array_final)
        print("Tempo de execução: %s segundos" % (final - inicio))


array = [(random.randint(0,100000)) for i in range(10000)]

#medição de tempo para trechos paralelo + sequencial
#inicio = time.time()
bubble_paralelo(array)
#final = time.time()

#print("Tempo de execução: %s segundos" % (final - inicio))