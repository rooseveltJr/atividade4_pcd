import matplotlib.pyplot as plt 

time_python = [0.085, 0.037, 0.014, 0.011]
threads= [1, 2, 4, 8]
time_c = [0.002, 0.002, 0.002, 0.003]
time_java = [0.024, 0.027, 0.027, 0.019]

plt.plot(threads, time_python, label="Python")
plt.plot( threads, time_java, label="Java")
plt.plot( threads, time_c, label="C")
plt.legend()
plt.ylabel("Tempo (s)")
plt.xlabel("Threads")
plt.title("Ordenacao vetor de 1000 posicoes")
plt.show()

time_python = [8.51, 4.69, 1.88, 1.36]
threads= [1, 2, 4, 8]
time_c = [0.10, 0.06, 0.062, 0.31]
time_java = [0.26, 0.096, 0.071, 0.061]

plt.plot(threads, time_python, label="Python")
plt.plot( threads, time_java, label="Java")
plt.plot( threads, time_c, label="C")
plt.legend()
plt.ylabel("Tempo (s)")
plt.xlabel("Threads")
plt.title("Ordenacao vetor de 10000 posicoes")
plt.show()

time_python = [861.35, 494.10, 192.09, 146.83]
threads= [1, 2, 4, 8]
time_c = [9.97, 5.07, 2.66, 7.87]
time_java = [52.98, 12.98, 3.28, 1.28]

plt.plot(threads, time_python, label="Python")
plt.plot(threads, time_java, label="Java")
plt.plot(threads, time_c, label="C")
plt.legend()
plt.ylabel("Tempo (s)")
plt.xlabel("Threads")
plt.title("Ordenacao vetor de 100000 posicoes")
plt.show()


threads= [1, 2, 4, 8]
time_c = [9.97, 5.07, 2.66, 7.87]

plt.plot(threads, time_c, label="C")
plt.legend()
plt.ylabel("Tempo (s)")
plt.xlabel("Threads")
plt.title("Ordenacao vetor de 100000 posicoes - C")
plt.show()


time_python = [861.35, 494.10, 192.09, 146.83]
threads= [1, 2, 4, 8]

plt.plot(threads, time_python, label="Python")
plt.legend()
plt.ylabel("Tempo (s)")
plt.xlabel("Threads")
plt.title("Ordenacao vetor de 100000 posicoes - Python")
plt.show()


threads= [1, 2, 4, 8]
time_java = [52.98, 12.98, 3.28, 1.28]

plt.plot(threads, time_java, label="Java")

plt.legend()
plt.ylabel("Tempo (s)")
plt.xlabel("Threads")
plt.title("Ordenacao vetor de 100000 posicoes")
plt.show()