const { Worker, isMainThread, parentPort, MessageChannel } = require('worker_threads')
const { port1, port2 } = new MessageChannel()

function bubble(array)  {
  for (i = 0; i < array.length - 1; i++) {
    for (j = i + 1; j < array.length; j++) {
      if (array[j] < array[i]) {
        const temp = array[j]
        array[j] = array[i]
        array[i] = temp
      }
    }
  }

  return array
}

function createSubLists (nThreads) {
  const lists = []
  let i = 0

  while(i < nThreads) {
    lists.push([])
    i++
  }
  
  return lists
}

function splitArrayInSublists (array, splitFactor, subLists, nThreads) {
  if (subLists.length === 1) {
    subLists[0] = subLists[0].concat(array)
    return subLists
  }
  for(i = 1; i <= nThreads; i++) {
    array = array.flatMap(value => {
      if (value <= splitFactor*i) {
        subLists[i - 1].push(value)
        return []
      } else {
        return value
      }
    })
  }

  return subLists
}

async function sortSubLists(subLists, nThreads) {
  let finishedWorkers = 0
  let result = createSubLists(nThreads)
  if (isMainThread) {
    for (let list of subLists) {
      const worker = new Worker(__filename)
      worker.on('error', console.error)
      
      worker.once('message', ({ sortedList, id }) => {
        result[id] = sortedList
        finishedWorkers++
        if (finishedWorkers === nThreads){
          port2.postMessage(result)
        }
      })

      const payload = { list, id: worker.threadId - 1 }
      worker.postMessage(payload)
    }
  } else {
    parentPort.once('message', ({ list, id }) => {
      const sortedList = bubble(list)
      const payload = { sortedList, id }
      parentPort.postMessage(payload)
    })
  }
}

function getSplitFactor (array, nThreads) {
  const max = Math.max(...array)
  return Math.floor(max/nThreads)
}

function concatSubLists (subLists) {
  let result = []
  subLists.forEach(list => {
    result = result.concat(list)
  })

  return result
}

async function splitArray (array) {
  const nThreads = 12
  let subLists = createSubLists(nThreads)
  const splitFactor = getSplitFactor(array, nThreads)
  const splitedArrayInSublists = splitArrayInSublists(array, splitFactor, subLists, nThreads)
  const start = Date.now()
  sortSubLists(splitedArrayInSublists, nThreads)
  if (isMainThread) {
    port1.on('message', (result) => {
      const end = Date.now()
      console.log(result.length)
      result.forEach(value => console.log(value.length))
      console.log(...concatSubLists(result))
      console.log(`Tempo de Execução: ${(end - start)}ms`)
    })
  }

}

const array = []
for (i = 0; i < 25000; i++) {
  array.push(Math.floor(Math.random() * (10000 - 0 + 1) + 0))
}
splitArray(array)  
