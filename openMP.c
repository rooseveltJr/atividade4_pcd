#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE 100000

int arr[ARRAY_SIZE];

void createArray()
{
    for(int i=0;i<ARRAY_SIZE;i++)
	{
	    arr[i]=ARRAY_SIZE - i;
	}
}

void swap(int *num1, int *num2)
{

	int temp = *num1;
	*num1 =  *num2;
	*num2 = temp;
}

int main (int argc, char *argv[]) {
	int i=0, j=0; 
	int first;
	double start,end;
    int n_threads;
    int n = ARRAY_SIZE;
    //printArray();
    printf("Insira a quantidade de Threads: \n");
    scanf("%d", &n_threads);
    omp_set_num_threads(n_threads);
	start=omp_get_wtime();
	for( i = 0; i < ARRAY_SIZE; i++ )
	{
		//first = i % 2; 
		#pragma omp parallel for default(none),shared(arr, n, i)
		for( j = i+1; j < n; j += 1 )
		{
			if( arr[ i ] > arr[ j ] )
			{
				swap( &arr[ i ], &arr[ j ] );
			}
		}
	}
    end=omp_get_wtime();

    printf("\nTempo Paralelo = %f",(end-start));    
    return 0;
}
