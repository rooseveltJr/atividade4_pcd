/* C program for Merge Sort */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#define ARRAY_SIZE 10000

int arr[ARRAY_SIZE];

int *result;

typedef struct arguments_thread{
    int init;
    int end;
    int index;
    int id;
} thread_arg;

void create_array()
{
    int i;
    for(i = 0; i < ARRAY_SIZE; i++)
    {
        arr[i] = (i * 13) % 7;
    }
}

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void *function_T(void *param)
{
    thread_arg values = *((thread_arg *) param);
    int i;
    int id_return = values.index;
    for(i = values.init ; i <= values.end ; i++)
    {
        if(arr[id_return] > arr[i])
        {
            //id_return = i;
            swap(&arr[id_return], &arr[i]);
        }
    }
    //result[values.id] = id_return;
    pthread_exit(NULL);
}

// A function to implement bubble sort
void bubbleSort_pthread(int n)
{
    int i, j, k;
    int block;
    int rest;
    int aux;
    pthread_t thread[n];
    thread_arg t_arg[n];
    for (i = 0; i < ARRAY_SIZE; i++)      
    {
        block = (ARRAY_SIZE - i)/ n;
        rest = (ARRAY_SIZE - i) % n;
        for(j = 0; j < n; j++)
        {
            t_arg[j].init = (j * block) + i;
            if(j == (n-1))
            {
                t_arg[j].end = ((j+1) * block) - 1 + i + rest;
            }
            else
            {
                t_arg[j].end = ((j+1) * block) - 1 + i;
            }
            t_arg[j].index = i;
            t_arg[j].id = j;
            pthread_create(&thread[j], NULL, function_T, (void *) &t_arg[j]);
        }
        for(j = 0; j < n; j++)
        {
            pthread_join(thread[j], NULL);
        }
        /*
        aux = result[0];
        for(j = 0; j < n; j++)
        {
            for(k = j + 1; k < n;k++)
            {
                if(arr[result[k]] < arr[aux])
                {
                    aux = result[k];
                    break;
                }
            }
        }
        if(aux != i)
            swap(&arr[i], &arr[aux]);
            */
    }
}

/* Function to print an array */
void printArray()
{
    int i;
    for (i=0; i < ARRAY_SIZE; i++)
        printf("%d ", arr[i]);
    printf("\n");
}

int main()
{
    int n_threads;
	create_array();
    printf("Insira a quantidade de Threads: \n");
    scanf("%d", &n_threads);
    result = (int *) malloc(sizeof(int) * n_threads);
    time_t begin = clock();
    bubbleSort_pthread(n_threads);
    time_t end = clock();
    printf("Sorted array: \n");
    printArray();
    printf("\n\nExecution Time: %f", ((double) (end - begin)/CLOCKS_PER_SEC));

	return 0;
}

